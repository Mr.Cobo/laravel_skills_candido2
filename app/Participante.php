<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string $nombre
 * @property string $apellidos
 * @property string $centro
 * @property string $tutor
 * @property string $fechaNacimiento
 * @property string $imagen
 * @property string $puntos
 * @property string $modalidad_id
 */
class Participante extends Model
{
    protected $table="participantes";

    public function modalidad()
    {
    	
    }
}
