<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modalidad extends Model
{
    protected $table="modalidades";

    public function participantes()
    {
    	return $this->hasMany(Participante::class,'modalidad_id','id');
    }

    public function participantesOrderByPuntos()
    {
    	return $this
    	->hasMany(Participante::class,'modalidad_id','id')
    	->orderBy('puntos','desc');
    }

     public function isPuntosOK()
    {
    	$p= $this
    	->hasMany(Participante::class,'modalidad_id','id');
    	return $p;
    	foreach($p as $participante)
    	{
    		if($participante->puntos != -1)
    			return $participante->puntos;
    	}
    	return false;
    }
}
